'use strict';
const request = require('request');
const fs = require('fs');
const URL = require('url');
const path = require('path');

/**
 * 解析域名
 * @param {*} url 
 */
const parseUrl = (url) => {
    return URL.parse(url);
}

// 匹配html文本中的icon地址
const extractIconLink = (htmlText) => {
    let shotCutReg = /\<link[^\n]*(rel=[\"']shortcut\sicon[\"']|type=\"image\/x-icon\")[^\n]*\>/;
    let iconPathReg = /href=[\"']([^\s]+)[\"']/;
    let shotCut = htmlText.match(shotCutReg);
    if (shotCut) {
        return shotCut[0].match(iconPathReg);
    }
    return false;
}

// 发送请求
const requestAssert = (path) => {
    console.log('get Icon from: ', path);
    return new Promise((resolve, reject) => {
        request(path, {
            encoding: null,
        }, (error, resp, body) => {
            if (error) {
                return reject(error);
            }
            let contentType = resp.headers['content-type'];
            if (contentType && contentType.indexOf('text') != -1) {
                fs.writeFileSync('./temp.html', body.toString('utf8'))
                return resolve({
                    type: 'text',
                    body: body.toString('utf8'),
                });
            } else if (contentType && contentType.indexOf('image') != -1) {
                return resolve({
                    type: 'icon',
                    body,
                });
            } else {
                return resolve({
                    type: false,
                    body,
                });
            }
        })
    });
}

// 转域名为icon文件名
const geneIconName = (hostname) => {
    return hostname.replace(/\./g, '_')
}

// 执行入口
let exec = (url) => {
    let iconName = '';
    return Promise.resolve(parseUrl(url)).then(urlObj => {
        iconName = geneIconName(urlObj.hostname);
        return requestAssert(`${urlObj.protocol}//${urlObj.hostname}/favicon.ico`);
    }).then(res => {
        if (!res.type || res.type == 'icon') {
            fs.writeFileSync(`./favicons/${iconName}.ico`, res.body);
            return Promise.resolve(true);
        }

        return requestAssert(url).then(res => {
            let iconRelativePath = extractIconLink(res.body);
            if (iconRelativePath) {
                return requestAssert(`${URL.resolve(url, iconRelativePath[1])}`);
            }
            throw new Error('icon not found!')
        }).then(res => {
            if (!res.type || res.type == 'icon') {
                fs.writeFileSync(`./favicons/${iconName}.ico`, res.body);
                return Promise.resolve(true);
            };
            throw new Error('icon not found!');
        })
    }).then(res => {
        if (res) {
            console.log('success');
        }
    }).catch(err => {
        console.log(err)
    });
}

module.exports = exec;

// let url = 'https://github.com/trending';
// exec(url);