# 获取网站图标icon

[toc]


获取网站图标的脚本

## Usage

```js
const getIcon = require('./index');
getIcon('https://www.baidu.com'); // return a promise
```

## 获取网站图标Icon的方法

### 1. 调用第三方api
- `http://statics.dnspod.cn/proxy_favicon/_/favicon?domain=url`
- `http://www.google.com/s2/favicons?domain=url`

### 2. 请求 `domain/favicon.ico`
    不是所有的网站适合
	这样请求相当于认为icon资源放在网站根目录下，但有些网站并不这样做

### 3. 从网页源代码中抽取
```css
<link rel="shortcut icon" href="gitbook/images/favicon.ico" type="image/x-icon">

<link rel="shortcut icon" type="image/x-icon" href="http://www.12306.cn/mormhweb/images/favicon.ico" />
```
从上述代码中获取href，如果是相对路径，拼接当前地址，请求icon。绝对路径直接请求

### 4. 执行源代码，抽取favicon.ico
如react应用，ico是js写进去的，没办法了只能运行js渲染页面后再抽取图标
例如： [如眸导航](http://sucheye.com/main/login?where=/)

	    
