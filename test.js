const getIcon = require('./index');

getIcon('https://www.baidu.com')
    .then(() => {
        return getIcon('https://github.com/trending')
    })
    .then(() => {
        return getIcon('http://www.12306.cn/')
    })
    .then(() => {
        return getIcon('https://www.cnblogs.com')
    })